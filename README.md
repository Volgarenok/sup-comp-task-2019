Build: 
```
$ make
```
Usage 1 (you should provide all parameters):
```
$ mpirun -n <num_of_nodes> ./task <amount>
```
Usage 2 (pass non-default parameters via Makefile variables):
```
$ make run NODES=<num_of_nodes> AMOUNT=<amount>
```
Examples: 
```
$ make run NODES=2 AMOUNT=100
$ mpirun -n 2 ./task 100
```

The program will generate <amount> numbers on <num_of_nodes> nodes. Your task is to sort them.