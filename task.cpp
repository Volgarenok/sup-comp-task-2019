#define OMPI_SKIP_MPICXX //I recommend not using C++ bindings
#include <mpi.h>
#include <omp.h>
#include <array>
#include <limits>
#include <memory>
#include <chrono>
#include <random>
#include <ostream>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <functional>

// Тип сортируемых значений (using <псевдоним> = <тип>)
// Не советую менять, так как типу unsigned int
// соответствует MPI_INT. Если поменяете, то имейте ввиду,
// что необходимо передать соответствующий тип в
// функции MPI при пересылке данных
using int_t = unsigned int;
namespace utility
{
  // Процесс и его описание: количество потоков, ранк
  // и размер коммуникатора (по-умолчанию коммуникатор MPI_COMM_WORLD)
  struct Process
  {
    int threads, rank, size;
    MPI_Comm comm;
    explicit Process(MPI_Comm c);
  };
  // Можно вывести в консоль. Имейте ввиду, что стандартные потоки IO
  // не потоко-безопасны
  std::ostream & operator<<(std::ostream & out, const Process & rhs);

  // Проверка отсортированности данных:
  // внутри узла и по отношению к другим узлам.
  // Указывается начало/конец массива и коммуникатор
  // Элементы массива: [ 0 ], [ 1 ] ... [ n ]
  // Указатели:  begin ^                end ^
  std::pair< bool, bool > is_sorted(
    const int_t * begin,
    const int_t * end,
    const MPI_Comm comm
    );

  // Функция сортировки, которую предложено реализовать
  // Принимает указатели на начало/конец массива, коммуникатор
  // и функцию сравнения
  // Элементы массива: [ 0 ], [ 1 ] ... [ n ]
  // Указатели:  begin ^                end ^
  void sort(
    int_t * begin,
    int_t * end,
    const MPI_Comm comm
    )
  {
    //I offer you to implemet this function
    //but feel free to reorganize code or
    //to ask questions about my program structure
    return;
  }
}

int main(int argc, char ** argv)
{
  auto provided = 0;
  //Инициализация MPI. std::addressof эквивалентно оператору '&'
  MPI_Init_thread(std::addressof(argc), std::addressof(argv), MPI_THREAD_SINGLE, std::addressof(provided));
  // Описатель процесса
  const auto pr = utility::Process(MPI_COMM_WORLD);
  // Два аргумента ./task   <amount of numbers>
  //   первый(имя) ^ второй ^
  if(argc != 2)
  {
    if(pr.rank == 0)
    {
      std::cerr << "Please, read README before using" << std::endl;
    }
    MPI_Finalize();
    return 0;
  }
  // Можно вывести в поток
  if(pr.rank == 0)
  {
    std::cout << pr << std::endl;
  }
  // Для второго аргумента: строка -> число
  auto amount = atoi(argv[1]);
  // Одинаковое количество элементов на каждом узле с точности до количества процессов/узлов
  const auto size_per_rank = (amount / pr.size) + ((pr.rank != (pr.size - 1)) ? 0 : (amount % pr.size));
  // Выделение памяти под данные
  auto data = std::unique_ptr< int_t[] >(new int_t[size_per_rank]);
  {
    // Генерация случайных чисел: левая и правая границы,
    // они же минимально и максимально возможные значения для типа int_t
    constexpr auto left_border = std::numeric_limits< int_t >::min();
    constexpr auto right_border = std::numeric_limits< int_t >::max();
    // Генератор. Вызываем как функцию
    namespace ch = std::chrono;
    auto dice = std::bind(
      std::uniform_int_distribution< int_t >{left_border, right_border},
      std::default_random_engine
      {
        ch::duration_cast< ch::duration< int_t > >(ch::system_clock::now().time_since_epoch()).count()
      }
    );
    std::generate_n(data.get(), size_per_rank,
		    [&](void)
		    {
		      // dice() вернёт "случайное" число
		      return dice();
		    });
  }
  // Вызов сортировки
  utility::sort(data.get(), data.get() + size_per_rank, MPI_COMM_WORLD);
  // Проверка отсортированности: в пределах узла (inner) и по отношению к другим узлам (outer)
  auto result = utility::is_sorted(data.get(), data.get() + size_per_rank, MPI_COMM_WORLD);
  auto inner = result.first;
  auto outer = result.second;
  if(inner && outer)
  {
    if(pr.rank == 0)
    {
      std::clog << "Data is sorted" << std::endl;
    }
  }
  else
  {
    if(pr.rank == 0)
    {
      std::clog << "Data is unsorted" << std::endl;
    }
  }
  // Не забываем вызвать
  MPI_Finalize();
  return 0;
}

namespace utility
{
  Process::Process(MPI_Comm c):
    threads(omp_get_max_threads()),
    rank(0),
    size(0),
    comm(c)
  {
    auto initialized = 0;
    // Проверяем, что инициализирован MPI
    MPI_Initialized(std::addressof(initialized));
    if(initialized)
    {
      // Получаем ранк
      MPI_Comm_rank(comm, std::addressof(rank));
      // Получаем размер коммуникатора
      MPI_Comm_size(comm, std::addressof(size));
    }
  }

  std::ostream & operator<<(std::ostream & out, const Process & rhs)
  {
    // Выводим описатель процесса в консоль
    out << "(:threads " << rhs.threads;
    out << ":rank " << rhs.rank;
    out << ":size " << rhs.size;
    out << ":)";
    return out;
  }

  std::pair< bool, bool > is_sorted(const int_t * begin, const int_t * end, const MPI_Comm comm)
  {
    // Компаратор по-умолчанию
    auto comp = std::less< int_t >{};
    auto mpi_type = MPI_INT;
    // Массив на узле отсортирован?
    auto inner_result = std::is_sorted(begin, end, comp);
    auto pr = Process{comm};

    auto outer_result = true;
    {
      // Собираем результаты о сортировках
      auto result = static_cast< int_t >(inner_result);
      auto others_results = std::unique_ptr< int_t[] >(new int_t[pr.size]);
      MPI_Allgather(std::addressof(result), 1, mpi_type, others_results.get(), 1, mpi_type, comm);
      auto others_total = std::all_of(others_results.get(), others_results.get() + pr.size,
				      [](const auto lhs)
				      {
					return lhs != 0u;
				      });
      outer_result &= others_total;
    }

    {
      constexpr auto mm_size = 2;
      // Минимум и максимум
      auto mm = std::array< int_t, mm_size >{*begin, *(begin + (std::distance(begin, end) - 1))};
      // Собираем данные о максимумах и минимумов со всех узлов
      auto others_mm = std::unique_ptr< int_t[] >(new int_t [pr.size * mm_size]);
      MPI_Allgather(mm.data(), mm_size, mpi_type, others_mm.get(), mm_size, mpi_type, comm);
      // Если пары [min max] [min max] ... упорядочены,
      // то считаем данные отсортированными
      auto others_mm_total = std::is_sorted(others_mm.get(), others_mm.get() + pr.size * mm_size, comp);
      outer_result &= others_mm_total;
    }
    return {inner_result, outer_result};
  }  
}
